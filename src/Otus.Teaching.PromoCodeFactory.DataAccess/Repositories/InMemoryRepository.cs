﻿using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T>
        : IRepository<T>
        where T : BaseEntity
    {
        protected IEnumerable<T> Data { get; set; }

        public InMemoryRepository(IEnumerable<T> data)
        {
            Data = data;
        }

        public Task<IEnumerable<T>> GetAllAsync()
        {
            return Task.FromResult(Data);
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
        }

        public Task DelByIdAsync(Guid id)
        {
            Data = Data.Where(x => x.Id != id).ToList();
            return Task.CompletedTask;
        }

        public Task AddAsync(T element)
        {
            Data = Data.Append(element);
            return Task.CompletedTask;
        }

        public Task UpdateAsync(T element)
        {
            Data = Data.Where(x => x.Id != element.Id).Append(element).ToList();
            return Task.CompletedTask;
        }
    }
}